const colors = {
  WHITE: '#ffffff',
  BLACK: '#000000',
  GRAYONE: '#eaeaea',
};

export default colors;
