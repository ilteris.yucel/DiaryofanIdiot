import {StyleSheet} from 'react-native';
import colors from '../constants/ColorConstants';

const styles = StyleSheet.create({
  SafeAreaViewStyle: {
    flex: 1,
    backgroundColor: colors.GRAYONE,
  },
  VerticalCenteredView: {
    flex: 1,
    justifyContent: 'center',
  },
  HorizontalCenteredView: {
    flex: 1,
    alignItems: 'center',
  },
  VerticalHorizontalCenteredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  VerticalCenteredScrollView: {
    flexGrow: 1,
    justifyContent: 'center',
  },
  HorizontalCenteredScrollView: {
    flexGrow: 1,
    alignItems: 'center',
  },
  VerticalHorizontalCenteredScrollView: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default styles;