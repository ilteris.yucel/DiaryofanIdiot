import React from 'react';
import MainStyles from './styles/MainStyles';

import {
  SafeAreaView,
  ScrollView,
  Text,
  View,
} from 'react-native';


const App =  () => {

  return (
    <SafeAreaView style={MainStyles.SafeAreaViewStyle}>
      <ScrollView contentContainerStyle={MainStyles.VerticalHorizontalCenteredScrollView}>
        <View>
          <Text>
            Hello World
          </Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default App;
